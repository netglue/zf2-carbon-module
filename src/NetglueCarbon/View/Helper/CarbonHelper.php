<?php

namespace NetglueCarbon\View\Helper;

use Carbon\Carbon;
use DateTime;
use Zend\View\Helper\AbstractHelper;

class CarbonHelper extends AbstractHelper
{

    public function __invoke()
    {
        return $this;
    }

    public function humanDiffFromNow(DateTime $when)
    {
        $when = Carbon::instance($when);
        return $when->diffForHumans();
    }
}
